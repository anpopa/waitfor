#include <assert.h>
#include <getopt.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#define SEC2MSEC(X) (X * 1000)
#define MSEC2USEC(X) (X * 1000)

static void terminate(int signum)
{
  exit(EXIT_SUCCESS);
}

static int do_waitfor(const char *fpath, int timeout, int sleepms)
{
  int max_rtry = SEC2MSEC(timeout) / sleepms;
  bool found = false;
  struct stat sb;

  assert(fpath);

  if (max_rtry <= 0) {
    if (stat(fpath, &sb) == 0) {
      if ((sb.st_mode & S_IFMT) == S_IFREG) {
        found = true;
      }
    }
  }

  while (!found && (max_rtry-- > 0)) {
    if (stat(fpath, &sb) == 0) {
      if ((sb.st_mode & S_IFMT) == S_IFREG) {
        found = true;
      }
    }
    if (!found) {
      usleep(MSEC2USEC(sleepms));
    }
  }

  if (found) {
    return 0;
  }

  return -1;
}

int main(int argc, char **argv)
{
  const char *fpath = NULL;
  int conf_timeout = 5;
  int conf_sleepms = 50;
  int long_index = 0;
  bool help = false;
  int c;

  struct option longopts[] = {{"timeout", required_argument, NULL, 't'},
                              {"sleepms", required_argument, NULL, 's'},
                              {"help", no_argument, NULL, 'h'},
                              {NULL, 0, NULL, 0}};

  while ((c = getopt_long(argc, argv, "t:s:h", longopts, &long_index)) != -1) {
    switch (c) {
    case 't':
      conf_timeout = strtoul(optarg, NULL, 10);
      break;
    case 's':
      conf_sleepms = strtoul(optarg, NULL, 10);
      break;
    case 'h':
      help = true;
      break;
    default:
      break;
    }
  }

  if (argc > 1) {
    fpath = argv[argc - 1];
  }

  if (help || (fpath == NULL) || (conf_sleepms == 0) || (SEC2MSEC(conf_timeout) < conf_sleepms)) {
    printf("waitfor: Wait for path entry to exist\n\n");
    printf("Usage: waitfor [OPTIONS] PATH\n\n");
    printf("  General:\n");
    printf("     --timeout, -t  <int> Wait timeout in seconds. Default 5s\n");
    printf("     --sleepms, -s  <int> Sleep time between polls waiting for path in miliseconds. "
           "Default 50ms\n");
    printf("  Help:\n");
    printf("     --help, -h           Print this help\n\n");
    exit(EXIT_SUCCESS);
  }

  signal(SIGINT, terminate);
  signal(SIGTERM, terminate);

  return do_waitfor(fpath, conf_timeout, conf_sleepms) == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
